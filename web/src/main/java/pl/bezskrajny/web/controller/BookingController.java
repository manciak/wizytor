package pl.bezskrajny.web.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.bezskrajny.model.BookingService;
import pl.bezskrajny.model.salon.SalonService;
import pl.bezskrajny.model.salon.Worker;

import java.util.Date;

@RestController
@RequestMapping("/booking")
public class BookingController {

    @ApiOperation(value = "Endpoint allowing to get all Bookings")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully received all free Bookings"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "No authorization"),
            @ApiResponse(code = 403, message = "No permission to access here"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @GetMapping
    public BookingService getAllBookings(SalonService service, Date day){
        return new BookingService(service,day);
    }


    @ApiOperation(value = "Endpoint allowing to get Bookings by particular worker")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully received free Bookings by worker"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "No authorization"),
            @ApiResponse(code = 403, message = "No permission to access here"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @GetMapping("/worker")
    public BookingService getBookingsByWorker(SalonService service, Date day, Worker worker){
        return new BookingService(service,day,worker);
    }


    @ApiOperation(value = "Endpoint allowing to set Booking")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully set booking"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "No authorization"),
            @ApiResponse(code = 403, message = "No permission to access here"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @PostMapping
    public BookingService setBooking(SalonService service, Date day){
        return new BookingService(service,day);
    }

    @ApiOperation(value = "Endpoint allowing to set Booking with particular worker")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully set booking with worker"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "No authorization"),
            @ApiResponse(code = 403, message = "No permission to access here"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
    @PostMapping("/worker")
    public BookingService setBookingWithWorker(SalonService service, Date day, Worker worker){
        return new BookingService(service,day,worker);
    }

}
