package pl.bezskrajny.model.salon.salonBasicInfo;

import lombok.Data;
import pl.bezskrajny.model.salon.salonBasicInfo.openingHours.*;

@Data
public class OpeningHours {

    private Monday monday;
    private Tuesday tuesday;
    private Wednesday wednesday;
    private Thursday thursday;
    private Friday friday;
    private Saturday saturday;
    private Sunday sunday;
}
