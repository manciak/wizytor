package pl.bezskrajny.model.salon.salonBasicInfo.openingHours;

import lombok.Data;

import java.util.Map;

@Data
public class Thursday {

    private Map<Integer,Integer> openingHours;
}
