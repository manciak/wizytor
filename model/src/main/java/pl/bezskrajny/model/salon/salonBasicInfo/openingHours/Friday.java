package pl.bezskrajny.model.salon.salonBasicInfo.openingHours;

import lombok.Data;

import java.util.Map;

@Data
public class Friday {

    private Map<Integer,Integer> openingHours;
}
