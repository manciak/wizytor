package pl.bezskrajny.model.salon;

import pl.bezskrajny.model.salon.salonBasicInfo.*;

import java.util.List;

public class Salon {

    private String name;
    private Worker owner;
    private List<BusinessType> businessTypes;
    private List<SalonService> salonServices;
    private Location location;
    private List<Integer> phoneNumbers;
    private AboutSalon aboutSalon;
    private List<Photo> photos;
    private List<Opinion> opinions;
    private SocialMedia socialMedia;
    private List<Worker> workers;
    private WorkingHours workingHours;



}
