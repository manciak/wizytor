package pl.bezskrajny.model.salon.salonBasicInfo;


public enum BusinessType {

    BARBER, NAILS, EYELASHES_EYEBROWS, MASSAGE, PHYSIOTHERAPY, COSMETIC, TATTOO, PIERCING, HOTEL

}
