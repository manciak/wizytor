package pl.bezskrajny.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.bezskrajny.model.salon.SalonService;
import pl.bezskrajny.model.salon.Worker;

import java.util.Date;

//todo logic for Booking
@AllArgsConstructor
@Getter
public class BookingService {

    private SalonService salonService;
    private Date visitDate;
    private Worker worker;

    public BookingService(SalonService salonService, Date visitDate) {
        this.salonService = salonService;
        this.visitDate = visitDate;
    }

    void getAllBookingsByDay(){

    }

    void getBookingsByWorker(){

    }


}
